﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Palette
{
    // the texture that colors are written to when Apply is called
    public Texture2D texture;
    [SerializeField]
    public Color32[] colors = new Color32[256];

    public Palette(Texture2D texture = null)
    {
        this.texture = texture != null ? texture : new Texture2D(16, 16);
    }

    public void SetColor(int index, Color32 color, bool apply = true)
    {
        colors[index] = color;

        if (apply) Apply();
    }

    public Color32 GetColor(int index)
    {
        return colors[index];
    }

    public void Apply()
    {
        texture.SetPixels32(colors);
        texture.Apply();
    }

    public void SetMaterialPalette(Material material)
    {
        material.SetTexture("_Palette", texture);
    }

    public void SetMaterialPalette(MaterialPropertyBlock block)
    {
        block.SetTexture("_Palette", texture);
    }
}
